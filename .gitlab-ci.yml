variables:
  PYTHONUSERBASE: ".local"
  IMAGE_PATH: "registry.gitlab.com/gpaw/gpaw-ci-containers"
  OMP_NUM_THREADS: "1"


default:
  before_script:
    - export PYTHONUSERBASE=`realpath $PYTHONUSERBASE`
    - mkdir -p `python -m site --user-site`
    - export PATH=$PYTHONUSERBASE/bin:$PATH
  interruptible: true


stages:
  - build
  - test


.build:
  stage: build
  before_script:
    - !reference [default, before_script]
    - pip install --user --no-deps git+https://gitlab.com/ase/ase.git@master
    - |
      cat << EOF > siteconfig.py
      parallel_python_interpreter = True
      mpi = True
      compiler = 'mpicc'
      libraries = []
      library_dirs = []
      include_dirs = []
      extra_compile_args = ['-fPIC',
                            '-O3',
                            '-g',
                            '-fopenmp',
                            '-Wall',
                            '-Werror',
                            ]
      extra_link_args = ['-fopenmp']

      libraries += ['blas']

      fftw = True
      libraries += ['fftw3']

      scalapack = True
      libraries += ['scalapack-openmpi']

      libraries += ['xc']
      EOF
  artifacts:
    when: always
    paths:
      - $PYTHONUSERBASE
      - gpaw.egg-info
      - _gpaw.*.so
      - "*.log"
    expire_in: 30 mins


.test:
  stage: test
  before_script:
    - !reference [default, before_script]
    - export PYTEST_ADDOPTS="--color=yes"


.gpu-job:
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_COMMIT_BRANCH =~ "/^gpu-.*/"


build:
  image: $IMAGE_PATH/build:oldest
  extends: .build
  tags:
    - linux
  script:
    - cat siteconfig.py
    - pip install --user --log build.log --editable .
    - gpaw -P 1 info
    - gpaw-python -m gpaw info


build-latest:
  image: $IMAGE_PATH/build:latest
  extends: .build
  tags:
    - linux
  script:
    - |
      cat << EOF >> siteconfig.py
      define_macros += [('NDEBUG', None)]
      undef_macros = []
      # TODO: PySys_SetArgv() etc deprecated in Python 3.11
      extra_compile_args += ['-Wno-deprecated-declarations']
      EOF
    - cat siteconfig.py
    - pip install --user --upgrade numpy scipy
    - pip install --user --log build.log --editable .
    - gpaw -P 1 info
    - gpaw-python -m gpaw info


build-cuda:
  image: $IMAGE_PATH/build-cuda:ubuntu-20.04-cuda-11.6.0
  extends:
    - .build
    - .gpu-job
  tags:
    - linux
  script:
    - |
      cat << EOF >> siteconfig.py
      gpu = True
      gpu_target = 'cuda'
      gpu_compiler = 'nvcc'
      gpu_compile_args = ['-O3',
                          '-g',
                          '-gencode', 'arch=compute_60,code=sm_60']
      libraries += ['cudart', 'cublas']
      EOF
    - cat siteconfig.py
    - pip install --user --log build.log --editable .


build-minimal:
  image: $IMAGE_PATH/build:oldest
  extends: .build
  tags:
    - linux
  script:
    - |
      cat << EOF > siteconfig.py
      mpi = False
      noblas = True
      nolibxc = True
      EOF
    - cat siteconfig.py
    - pip install --user --log build.log --editable .
    - gpaw info


prepare-libxc:
  image: $IMAGE_PATH/build:oldest
  stage: build
  tags:
    - linux
  variables:
    PREFIX: $PYTHONUSERBASE
  before_script:
    - export PREFIX=`realpath $PREFIX`
  script:
    - git clone -b 6.2.2 https://gitlab.com/libxc/libxc.git
    - cd libxc
    - autoreconf -i
    - >
      ./configure --enable-shared --disable-static --disable-fortran --prefix=$PREFIX
      --disable-kxc
      --disable-lxc
      # Enable kxc, lxc, ... derivatives only if needed (affects compilation time)
    - make | tee ../libxc-build.log
    - make install
  when: manual
  artifacts:
    paths:
      - $PREFIX
      - "*.log"
    expire_in: 30 mins


build-libxc:
  image: $IMAGE_PATH/build:oldest
  extends:
    - .build
  needs:
    - prepare-libxc
  tags:
    - linux
  script:
    - |
      cat << EOF >> siteconfig.py
      local = Path('.local').resolve()
      include_dirs += [local / 'include']
      library_dirs += [local / 'lib']
      runtime_library_dirs += [local / 'lib']
      EOF
    - cat siteconfig.py
    - pip install --user --log build.log --editable .
    - gpaw -P 1 info


test:
  image: $IMAGE_PATH/run:oldest
  extends:
    - .test
  needs:
    - build
  tags:
    - linux
  script:
    - gpaw info
    - OMP_NUM_THREADS=2 pytest -v -m ci -We -Wi::ImportWarning
  # This is the only test which uses OpenMP.
  # We may want to identify a limited set of tests that must work
  # with OpenMP, but so far we just have it in this job.

    - gpaw-python -m gpaw info
    - >
      OMP_NUM_THREADS=2 gpaw-python -m pytest -v -m ci
      -We -Wi::ImportWarning
      -Wi:'numpy.ufunc size changed, may indicate binary incompatibility. Expected 192 from C header, got 216 from PyObject':RuntimeWarning


test-latest:
  image: $IMAGE_PATH/run:latest
  extends:
    - .test
  needs:
    - build-latest
  tags:
    - linux
  script:
    - pip install coverage pytest-cov
    - gpaw info

    # Create a .coveragerc file so we can get nicely printed coverage:
    - |
      cat << EOF >> .coveragerc
      [report]
      precision = 2
      EOF

    - >
      pytest -v -m ci
      -We -Wi::ImportWarning
      --cov=gpaw --cov-report=html --cov-report=term

    - gpaw-python -m gpaw info
    - >
      OMP_NUM_THREADS=2 gpaw-python -m pytest -v -m ci
      -We -Wi::ImportWarning
  artifacts:
    paths:
      - htmlcov/
    expire_in: 1 week
  coverage: '/TOTAL.+ ([0-9]+\.[0-9]+%)/'


test-minimal:
  image: $IMAGE_PATH/run:oldest
  extends:
    - .test
  needs:
    - build-minimal
  tags:
    - linux
  script:
    - gpaw info
    - pytest -v -m ci -We -Wi::ImportWarning


test-cuda:
  image: $IMAGE_PATH/run-cuda:ubuntu-20.04-cuda-11.6.0
  extends:
    - .test
    - .gpu-job
  needs:
    - build-cuda
  tags:
    - cuda
  script:
    - nvidia-smi
    - gpaw info
    - pytest -v -m gpu

    - gpaw-python -m gpaw info
    - gpaw-python -m pytest -v -m gpu


test-libxc:
  image: $IMAGE_PATH/run:oldest
  extends:
    - .test
  needs:
    - build-libxc
  tags:
    - linux
  script:
    - gpaw info
    - pytest -v -m libxc


check-agts:
  image: $IMAGE_PATH/run:oldest
  stage: test
  needs:
    - build
  tags:
    - linux
  script:
    - pip install git+https://gitlab.com/myqueue/myqueue.git@master
    - mq config --in-place
    - mq init
    - mq workflow -p agts.py -zT | tail -1 | tee task_count
    - >
      [[ `cut -d ' ' -f1 < task_count` -ge 479 ]] || (echo "Too few agts tasks"; exit 1)


lint:
  image: $IMAGE_PATH/run:latest
  stage: build
  tags:
    - linux
  script:
    - echo "png check"
    - >
      [[ `find . -name '*.png' | wc -l` -le 5 ]] || (echo "Too many png files in git"; exit 1)
    - echo "compileall"
    - python --version
    - python -We:invalid -m compileall -f -q gpaw/
    - echo "flake8"
    - pip install flake8
    - flake8 --version
    - flake8 --doctests gpaw
    - >
      flake8
      --doctests
      --exclude "doc/platforms/*,doc/*/summerschool22/*/"
      --extend-ignore E402
      doc
    - >
      flake8
      --doctests
      --extend-ignore E402,E501
      doc/summerschools/summerschool22/catalysis
    - echo "interrogate"
    - pip install interrogate
    - interrogate --version
    - >
      interrogate -m -i
      -f 33.1
      -e gpaw/test
      -e gpaw/point_groups/groups.py
      gpaw


typecheck:
  image: $IMAGE_PATH/run:latest
  stage: build
  tags:
    - linux
  script:
    - pip install mypy types-PyYAML
    - python --version
    - mypy --version
    - mypy --pretty


docs:
  image: $IMAGE_PATH/docs:oldest
  stage: test
  needs:
    - build
  tags:
    - linux
  script:
    - cd doc
    - make
    - make doctest
    - cd ..
    - python -c "from gpaw.utilities.urlcheck import test; test()"
  when: manual
  artifacts:
    paths:
      - doc/build/html


.nightly:
  image: $IMAGE_PATH/run:latest
  variables:
    GPAW_MPI_NPROCS: "1"
    PYTEST_TIMEOUT: "300"
  extends:
    - .test
  stage: test
  needs:
    - build-latest
  tags:
    - linux
  before_script:
    - !reference [default, before_script]
    - pip install pytest-instafail pytest-xdist pytest-timeout
  script:
    - gpaw -P $GPAW_MPI_NPROCS python -m pytest --instafail -x -r s
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"


nightly-serial:
  extends:
    - .nightly
  script:
    - gpaw info
    - pytest -n 4 --instafail -r s


nightly-mpi-2:
  variables:
    GPAW_MPI_NPROCS: "2"
  extends:
    - .nightly


nightly-mpi-4:
  variables:
    GPAW_MPI_NPROCS: "4"
  extends:
    - .nightly


nightly-mpi-8:
  variables:
    GPAW_MPI_NPROCS: "8"
  extends:
    - .nightly
